package ersterTest;

import Niffler.Niffler;

public class NifflerFutter
{

  public NifflerFutter()
  {
  }

  public static void main(String[] args)
  {
    Niffler niff=new Niffler();
    System.out.println("es gibt jetzt "+niff.getCounter()+" Niffler-Instanzen.");
    Niffler ler=new Niffler("Ler", 7, 28);
    System.out.println("es gibt jetzt "+ler.getCounter()+" Niffler-Instanzen.");
    System.out.println(niff.toString());
    System.out.println(ler.toString());
    System.out.println("Nach dem Looten:");
    niff.goldGeben(13);
    ler.goldGeben(21);
    System.out.println(niff.toString());
    System.out.println(ler.toString());
    System.out.println("es gibt jetzt "+ler.getCounter()+"/"+niff.getCounter()+" Niffler-Instanzen.");
    
  }

}
